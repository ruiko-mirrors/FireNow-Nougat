package android.app;

import android.util.Log;
import android.os.IFireflyManager;
import android.content.Context;
import android.os.RemoteException;
import android.os.Build;

import android.firefly.util.PowerOnOffUtils;
import android.firefly.util.SystemInfoUtils;
import android.firefly.util.SystemUtils;

public class FireflyManager {
    private static String TAG = "FireflyManager";

    IFireflyManager mFireflyManager;
    private Context context;
    public FireflyManager(Context ctx,IFireflyManager fireflyManager) {
        mFireflyManager = fireflyManager;
        context = ctx;
    }

    /*******
     * 
     * 系统相关信息获取
     * 
     */

    /***
     * 获取目前设备的型号　
     */
    public  String getAndroidModel(){
        return Build.MODEL;
    }

    /***
     * 获取目前设备的android系统的版本
     */
    public  String getAndroidVersion(){
        return Build.VERSION.RELEASE;
    }

    /***
     * 获取设备的RAM大小,单位为MB
     */ 
    public  long getRamSpace (){
        return SystemInfoUtils.getRamSpace();

    }

    /***
     * 获取设备的RAM大小，并格式化为String格式
     */ 
    public  String getFormattedRamSpace(){
        return SystemInfoUtils.getFormattedRamSpace(context);
    }

    /***
     * 获取设备的内置Flash大小,单位为MB
     */
    public  long getFlashSpace(){
        return SystemInfoUtils.getFlashSpace();
    }
    /***
     * 获取设备的内置Flash大小,并格式化为String格式
     */
    public  String getFormattedFlashSpace(){
        return SystemInfoUtils.getFormattedFlashSpace(context);
    }

    /***
     * 获取设备的固件SDK版本
     * 有疑问，待确认
     */
    public  String getFirmwareVersion(){
        return null;
    }

    /***
     * 获取设备的固件内核版本。
     */
    public  String getFormattedKernelVersion(){
        return SystemInfoUtils.getFormattedKernelVersion();
    }
    /***
     * 获取设备的固件系统版本和编译日期。
     */ 
    public String getAndroidDisplay (){
        return Build.DISPLAY;
    }

   /*******
     * 
     * 开关机
     * 
     */
    /***
     * 系统关机
     * @param confirm is true 弹出是否关机的确认窗口 
     */
    public void shutdown(boolean confirm)
    {
        try {
            mFireflyManager.shutdown(confirm);
        } catch (RemoteException e) {
            
        }
    }

    /***
     * 系统重启
     * @param confirm is true 弹出是否关机的确认窗口 
     * @param reason  is recovery or null
     */
    public void reboot(boolean confirm,String reason)
    {
        try {
            mFireflyManager.reboot(confirm,reason);
        } catch (RemoteException e) {
            
        }
    }

    /***
     * 系统休眠
     */
    public void sleep()
    {
        try {
            mFireflyManager.sleep();
        } catch (RemoteException e) {
            
        }
    }


    /*******
     * 
     * 系统设置
     * 
     */
    public  void  takeScreenshot(String path,String name)
    {
        try {
            mFireflyManager.takeScreenshot(path,name);
        } catch (RemoteException e) {
            
        }
    }

    public  void setRotation (int  rotation)
    {
        try {
            mFireflyManager.setRotation(rotation);
        } catch (RemoteException e) {
            
        }
    }


    public  int getRotation ( )
    {
        try {
            return mFireflyManager.getRotation();
        } catch (RemoteException e) {
            
        }
        return 0;
    }
    public  void thawRotation ( )
    {
        try {
            mFireflyManager.thawRotation();
        } catch (RemoteException e) {
            
        }
    }


    public void setStatusBar(boolean show)
    {
        try {
            mFireflyManager.setStatusBar(show);
        } catch (RemoteException e) {
            
        }
    }


}